package tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import classes.Boat_type;
import classes.Boat;
import dao.Boat_typeDAO;
import dao.BoatDAO;

public class TestDAO {

	public static void main(String[] args) {
		
		
		BoatDAO boat = new BoatDAO();
		Boat_typeDAO boat_type = new Boat_typeDAO();
		
		
		
		System.out.println("Récupération des données de la table Embarcation : \n ");
		for (Boat e : boat.recupAll())
			System.out.println(e.toString());
		System.out.println("<---------------------------------------------------------------------------------------------------------------------------------->\n\n");
		
		
		System.out.println("Récupération des données de la table Type_Embarcation : \n");
		for (Boat_type t : boat_type.recupAll())
			System.out.println(t.toString());
		System.out.println("<---------------------------------------------------------------------------------------------------------------------------------->\n\n");
		
		boat_type.searchiD("barque 2 places");
		System.out.println("<---------------------------------------------------------------------------------------------------------------------------------->\n\n");
		
		
		System.out.println("Récupération des données de la table Embarcation : \n ");
		for (Boat e : boat.searchBoatAvailable(new java.sql.Date(2020-06-15),new java.sql.Time(14),new java.sql.Time(20),boat_type.searchiD("pédalo simple")))
			System.out.println(e.toString());
		System.out.println("<---------------------------------------------------------------------------------------------------------------------------------->\n\n");
        
	}
}