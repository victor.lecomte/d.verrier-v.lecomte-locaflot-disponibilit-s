package classes;

import java.lang.String;

/**
 * Definition of the rent class, as defined in the DB
 *  with the attributes in english 
 * @author Victor LECOMTE and VERRIER Dorian
 */
public class Rent {
	//Attributes
	private int contract_id;
	private String boat_id;
	private int persons_id;
	
	// Accessors (getters)
	/**
     * Allows the recovery of the contract_id 
     * @return Integer representing the contract_id
     */
	public int getContract_id() {
		return contract_id;
	}
	
	/**
     * Allows the recovery of the boat_id 
     * @return String representing the boat_id
     */
	public String getBoat_id() {
		return boat_id;
	}
	
	/**
     * Allows the recovery of the persons_id 
     * @return Integer representing the persons_id
     */
	public int getPersons_id() {
		return persons_id;
	}

	// Mutator (setter)
	
	/**
     * Allows the modification of the contract_id  
     * @param contract_id
     */
	public void setContract_id(int contract_id) {
		this.contract_id = contract_id;
	}

	/**
     * Allows the modification of the boat_id  
     * @param boat_id
     */
	public void setBoat_id(String boat_id) {
		this.boat_id = boat_id;
	}
	
	/**
     * Allows the modification of the persons_id  
     * @param persons_id
     */
	public void setPersons_id(int persons_id) {
		this.persons_id = persons_id;
	}
	
	// Default constructor
	/**
	 * Default constructor
	 */
    public Rent() {
    	super();
    	contract_id = 0;
    	boat_id = "";
    	persons_id = 0;
    }

	
	// Constructor with parameters
    /**
     * Constructors with differents parameters
     * @param contract_id the contract_id of a rent
     * @param boat_id the boat_id of a rent
     * @param persons_id the persons_id of a rent
     */
	public Rent(int contract_id, String boat_id, int persons_id) {
		super();
		this.contract_id = contract_id;
		this.boat_id = boat_id;
		this.persons_id = persons_id;
	}

	// Methods
	/**
	 * Allows the display of all parameters (attributes)
	 */
	@Override
	public String toString() {
		return "L'id du contrat est : "+ contract_id 
				+ " puis l'id de l'embarcation est : " + boat_id 
				+ " et pour finir le nombre de personnes est : " + persons_id;
	}
	
}