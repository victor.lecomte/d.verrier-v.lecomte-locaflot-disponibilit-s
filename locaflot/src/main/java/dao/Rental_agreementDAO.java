package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.*;;

/**
* Definiton of class boatDAO
* @author Victor LECOMTE and VERRIER Dorian
*
*/
public abstract class Rental_agreementDAO  {

	public Connection connect = ConnexionPostgreSql.getInstance();
	
	/**
	 * Definition of the recupAll() method of the class rental_agreementDAO
	 */
	public List<Rental_agreement> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Rental_agreement> listContract = new ArrayList<Rental_agreement>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"locaflot_dispo\".contrat_location");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table embarcation
				Rental_agreement aContract = new Rental_agreement();
				
				aContract.setId(curseur.getInt("id"));
				aContract.setDate(curseur.getDate("date"));
				aContract.setStart_time(curseur.getTime("heure_debut"));
				aContract.setEnd_time(curseur.getTime("heure_fin"));
			
			
				listContract.add(aContract);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listContract; 
	}
}