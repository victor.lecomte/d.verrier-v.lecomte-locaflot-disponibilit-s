package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import classes.*;;

/**
* Definiton of class boatDAO
* @author Victor LECOMTE and VERRIER Dorian
*
*/
public class BoatDAO  {

	public Connection connect = ConnexionPostgreSql.getInstance();
	
	/**
	 * Definition of the recupAll() method of the class boatDAO
	 */
	public List<Boat> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Boat> listBoat = new ArrayList<Boat>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"locaflot_dispo\".embarcation");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table embarcation
				Boat aBoat = new Boat();
				
				aBoat.setId(curseur.getString("id"));
				aBoat.setColor(curseur.getString("couleur"));
				aBoat.setAvailable(curseur.getBoolean("disponible"));
				aBoat.setType(curseur.getString("type"));
			
			
				listBoat.add(aBoat);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listBoat; 
	}
	
	
	public List<Boat> searchBoatAvailable(Date date,Time start_time,Time end_time,String type) {
		// définition de la liste qui sera retournée en fin de méthode
		List<Boat> listBoat = new ArrayList<Boat>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from locaflot_dispo.embarcation WHERE id NOT IN\n" + 
					"					( SELECT id_embarcation \n" + 
					"					FROM locaflot_dispo.type_embarcation te  \n" + 
					"					LEFT JOIN locaflot_dispo.embarcation e ON e.type = te.code  \n" + 
					"					LEFT JOIN locaflot_dispo.louer l ON l.id_embarcation = e.id \n" + 
					"					LEFT JOIN locaflot_dispo.contrat_location cl ON cl.id = l.id_contrat\n" + 
					"					WHERE date = '"+date+"' AND heure_debut = '"+start_time+"' AND heure_fin = '"+end_time+"' AND type = 'B1')\n" + 
					"AND type = '"+type+"';");   

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table embarcation
				Boat aBoat = new Boat();
				
				aBoat.setId(curseur.getString("id"));
				aBoat.setColor(curseur.getString("couleur"));
				aBoat.setAvailable(curseur.getBoolean("disponible"));
				aBoat.setType(curseur.getString("type"));
			
			
				listBoat.add(aBoat);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listBoat; 
	}

}