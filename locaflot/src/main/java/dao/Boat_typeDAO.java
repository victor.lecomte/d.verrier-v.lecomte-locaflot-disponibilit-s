package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import classes.*;;

/**
* Definiton of class boatDAO
* @author Victor LECOMTE and VERRIER Dorian
*
*/
public class Boat_typeDAO  {

	public Connection connect = ConnexionPostgreSql.getInstance();
	
	/**
	 * Definition of the recupAll() method of the class boat_typeDAO
	 */
	public List<Boat_type> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Boat_type> listBoat = new ArrayList<Boat_type>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"locaflot_dispo\".type_embarcation");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table embarcation
				Boat_type aBoat_type = new Boat_type();
				
				aBoat_type.setCode(curseur.getString("code"));
				aBoat_type.setName(curseur.getString("nom"));
				aBoat_type.setNb_place(curseur.getInt("nb_place"));
				aBoat_type.setHalf_hour_price(curseur.getBigDecimal("prix_demi_heure"));
				aBoat_type.setHalf_hour(curseur.getBigDecimal("prix_heure"));
				aBoat_type.setHalf_day(curseur.getBigDecimal("prix_jour"));
				aBoat_type.setHalf_day_price(curseur.getBigDecimal("prix_demi_jour"));
			
			
				listBoat.add(aBoat_type);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listBoat; 
	}
	
	// Recherche d'un Licencie par rapport à son code	
	public String searchiD (String nom) {
		
		String code = null;
		
			try {
		          ResultSet result = this.connect
		                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
		                                                ResultSet.CONCUR_UPDATABLE)
		                             .executeQuery("select * from \"locaflot_dispo\".type_embarcation "
		                             		+ "WHERE nom = '" + nom +"'");
		          
		          if(result.first())
		           		code = result.getString("code");
		          
		    }
			catch (SQLException e) {
				        e.printStackTrace();
			
					}
			System.out.println("Le nom de cette embarcation correspond au code : " + code);
			return code;
	}
}